package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/jacobsa/go-serial/serial"
)

type Reading struct {
	PM25     float32
	PM10     float32
	DeviceID int
	Checksum int
}

func FromBytes(b []byte) (Reading, error) {
	res := Reading{}
	if len(b) != 10  && len(b) != 17 {
		return res, fmt.Errorf("len too short: %d", len(b))
	}
	log.Printf("FromBytes: num of bytes: %d", len(b))
	pm25 := 2
	pm10 := 4
	checksum := 8
	deviceID := 6
	if len(b) > 10 {
		checksum = 17
	}
	res.PM25 = float32(binary.LittleEndian.Uint16(b[pm25 : pm25+2]))/10.0
	res.PM10 = float32(binary.LittleEndian.Uint16(b[pm10 : pm10+2]))
	res.DeviceID = int(binary.LittleEndian.Uint16(b[deviceID : deviceID+2]))
	res.Checksum = int(b[checksum])
	return res, nil
}

func (r Reading) String() string {
	res := bytes.Buffer{}
	fmt.Fprintf(&res, "Device: %d, PM25: %f, PM10: %f", r.DeviceID, r.PM25, r.PM10)
	return res.String()
}

func runMonitor(w io.Writer, r io.Reader) error {
	rlog := log.New(w, "", log.LstdFlags)
	for {
		var b [19]byte
		// Read from port.
		n, err := r.Read(b[:])
		if err != nil {
			return fmt.Errorf("error reading bytes: %w", err)
		}
		// if n != len(b)
		// fmt.Fprintf(w, "# bytes: %d, data: %#v\n", n, b[:n])
		// Write to std out, formatted
		m, err := FromBytes(b[:n])
		if err != nil {
			log.Printf("error converting readings: %v", err)
			continue
		}

		rlog.Printf("Readings: %s", m.String())
	}
}

func logReading(l *log.Logger, r *Reading) {
	log.Println(r.String())
}

const serialPort = "/dev/ttyUSB0"

func run() error {
	// Open port
	sp, err := serial.Open(serial.OpenOptions{
		PortName:              serialPort,
		BaudRate:              9600,
		DataBits:              8,
		StopBits:              1,
		ParityMode:            serial.PARITY_NONE,
		InterCharacterTimeout: 1,
		MinimumReadSize:       1,
	})
	if err != nil {
		return fmt.Errorf("error opening %s: %w", serialPort, err)
	}
	return runMonitor(os.Stdout, sp)
}

func main() {
	if err := run(); err != nil {
		log.Printf("error run: %v", err)
	}
}
